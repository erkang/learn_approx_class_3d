------- Dataset Folder -------

Put in this folder a .txt file containing the filter bank to approximate.

Please follow the following conventions:
The .txt file should contain the whole filter bank saved piling up the rows.
More precisely, if N is the number of learned filters and R,C,S are respectively the number rows, columns and slices of the filters, then the .txt file should have C columns and R*S*N rows, where the first R*S rows correspond to the first filter (first row corresponding to first row of the first slice, second row to the second row of the first slice, … , R-th row to the last row of the first slice, …, R*S-th row corresponding to the last row of the last slice), rows from R*S+1 to 2*R*S correspond to the second filter and so on. 

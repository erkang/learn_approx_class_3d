function [t] = optimize_coeffs(p,M,x)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

% Initialize coeffs
t = M'*x;
    
for i_gd = 1:p.gradient_steps_no
    
    %compute residual
    res = x-M*t;
        
    %compute gradient
    grad_coeffs = -2*M'*res*p.gradient_step_size;
    
    %update coeff
    t = t-grad_coeffs;
        
    % soft thresholding on coeffs
    t(abs(t) < p.lambda_l1) = 0;
    t(t >= p.lambda_l1) = t(t >= p.lambda_l1)-p.lambda_l1;
    t(t <= -p.lambda_l1) = t(t <= -p.lambda_l1)+p.lambda_l1;
end

end

function [M] = normalize_fb(p, M)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

for i_f = 1 : p.filters_no
    M(:, i_f) = M(:, i_f)/norm(M(:, i_f));
end

end

function [p,M] = apply_componentwise_NN_proximal_operator3D(p, M)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

for i_f = 1 : p.filters_no
    
    %compute CP decomposition of filters; starting point for optimization is the solution of
    %the previous iteration
    %cp_opt gives better results than cp_als
    %P = cp_als( tensor(reshape( M(:, i_f), p.filters_size)) ,p.rank, 'init', p.CPdec{i_f}.U, 'printitn',0);
    P = cp_opt( tensor(reshape( M(:, i_f), p.filters_size )) ,p.rank, 'init', p.CPdec{i_f}.U,'alg','ncg');
    
    % soft thresholding step on coefficients
     s = P.lambda;
     s(abs(s) < p.lambda_nuclear) = 0;
     s(s >= p.lambda_nuclear) = s(s >= p.lambda_nuclear)-p.lambda_nuclear;
     s(s <= -p.lambda_nuclear) = s(s <= -p.lambda_nuclear)+p.lambda_nuclear;
    
        
    recomp = ktensor(s,P.U);  %tensor already arranged 
    
    %p.CPdec{i_f} = recomp; % will be the starting point to compute the next CP decomposition (with cp_als)         
    
    %to include updated lambda in starting point (only with cp_opt)
    P.U{1} = khatrirao((s').^(1/3),P.U{1});
    P.U{2} = khatrirao((s').^(1/3),P.U{2});
    P.U{3} = khatrirao((s').^(1/3),P.U{3});
    p.CPdec{i_f} = ktensor(ones(p.rank,1),P.U); 
                                                                                    
     
    %update coefficients matrix
    recomp = double(recomp);
    M(:, i_f) = recomp(:);
    
    
end

end

function [] = save_filter_bank(p, M, count)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

%write fb in txt format
output_path = fullfile(p.filters_txt_directory, sprintf('fb_%06d.txt', count/p.steps_no));

%write first filter overwriting preexistent files
filter = reshape(M(:, 1), p.filters_size);

dlmwrite(output_path, filter(:,:,1), 'delimiter', '\t', 'precision', 16);
for i_sli = 2:p.filters_size(3),
    dlmwrite(output_path, filter(:,:,i_sli), 'delimiter', '\t', 'precision', 16, '-append');
end

for i_f = 2 : p.filters_no
    filter = reshape(M(:, i_f), p.filters_size);
    
    for i_sli = 1:p.filters_size(3),
       dlmwrite(output_path, filter(:,:,i_sli), 'delimiter', '\t', 'precision', 16, '-append');
    end
    
end

%write only separable components 
output_sep_path = fullfile(p.filters_sep_directory, sprintf('fb_sep_%06d.txt', count/p.steps_no)); 
A = zeros(p.filters_size(1),p.filters_no);
B = zeros(p.filters_size(2),p.filters_no);
C = zeros(p.filters_size(3),p.filters_no);

for i_f =1:p.filters_no,
    A(:,i_f)=p.CPdec{i_f}.U{1}(:,1); % keep only first component (if converging the others should be zero)
    B(:,i_f)=p.CPdec{i_f}.U{2}(:,1);
    C(:,i_f)=p.CPdec{i_f}.U{3}(:,1);
end

dlmwrite(output_sep_path, A, 'delimiter', '\t', 'precision', 16); % overwrite existing file
dlmwrite(output_sep_path, B, 'delimiter', '\t', 'precision', 16,'-append');
dlmwrite(output_sep_path, C, 'delimiter', '\t', 'precision', 16,'-append');


end

function [t] = best_optimize_coeffs(p,M,x,tol)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

% Initialize coeffs
t = M'*x;
dim = length(t);

improvement = inf;
last_err = 0;
it_no = 1;

% Optimize coeffs
while(improvement>tol)
    res = x-M*t;
    if(it_no>1)
        improvement = abs(last_err-norm(res)/dim);
    end
    last_err = norm(res)/dim;
    
    grad_coeffs = -2*M'*res*p.gradient_step_size;
    t = t-grad_coeffs;
    
    % soft thresholding on coeffs
    t(abs(t) < p.lambda_l1) = 0;
    t(t >= p.lambda_l1) = t(t >= p.lambda_l1)-p.lambda_l1;
    t(t <= -p.lambda_l1) = t(t <= -p.lambda_l1)+p.lambda_l1;
    it_no = it_no+1;
end

end
3D Filter Learning and Pixel Classification framework.

This code implements the framework presented in [1]. It is composed of 3 independent parts:

1. convolutional_filter_learning_3D: learn a 3D convolutional filter bank on a dataset.
2. approx_fb_cpd: approximate a 3D filter bank with learned separable filters.
3. pixel_classification3D: classification of tubular structures in 3D images.

Details about the three components of the framework are given in the respective
subdirectories.

For any question or bug report, please feel free to contact me at:
amos <dot> sironi <at> epfl <dot> ch

If you use this code in your project, please cite our paper [1].

[1] R. Rigamonti, A. Sironi, V. Lepetit and P. Fua. Learning Separable Filters. IEEE International Conference on Computer Vision and Pattern Recognition (CVPR 2013), Portland, Oregon, USA, 2013.

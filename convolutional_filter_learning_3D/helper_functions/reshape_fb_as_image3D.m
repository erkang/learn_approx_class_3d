function reshape_fb_as_image3D(output_nrrd_name , input_txt_name, filters_no, filters_size, pixS, space)
% reshape_fb_as_image3D
% trasform a filter bank from txt format into a
% unique nrrd image, for visualization

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

if nargin < 5, pixS = 2; end
if nargin < 6, space = [4,4]; end


fb = load(input_txt_name);

n_blocks = filters_no;
n_block_rows = ceil(sqrt(n_blocks));
n_block_cols = ceil(sqrt(n_blocks));

img = 255*ones(n_block_rows*filters_size(1)*pixS + (n_block_rows-1)*space(1), ...
               n_block_cols*filters_size(2)*pixS + (n_block_cols-1)*space(2), ...
               filters_size(3)*pixS);

rowN = 1;
colN = 1;
for f = 1 : filters_no,
    filter_temp = fb((f-1)*filters_size(1)*filters_size(3)+1:f*filters_size(1)*filters_size(3), :); 
    filter = permute(reshape(filter_temp',[filters_size(2),filters_size(1),filters_size(3)]),[2,1,3]); 

    filter = magnify_matrix(filter, pixS); 
    
    filter = convert_img_visualization(filter);
    
    if(colN == n_block_cols+1)
        rowN = rowN+1;
        colN = 1;
    end
    
    img((rowN-1)*filters_size(1)*pixS + (rowN-1)*space(1) + 1 : rowN*filters_size(1)*pixS + (rowN-1)*space(1), ...
        (colN-1)*filters_size(2)*pixS + (colN-1)*space(2) + 1 : colN*filters_size(2)*pixS + (colN-1)*space(2), ...
            :) = filter;
    colN = colN+1;
end

nrrdSave(output_nrrd_name, permute(img,[2,1,3]));

function [p] = get_config_3D()
%  get_config_3D  setup the configuration for the convolutional filter
%              learning framework
%
%  Synopsis:
%     [p] = get_config_3D()
%
%  Ouput:
%     p = structure containing the parameters required by the system

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

p.dimension =3;
%% Dataset's parameters
% File containing the path for the training images (N.B. images should have .nrrd extension)
p.dataset_filelist = 'datasets/YOUR_NRRD_IMAGES_textlist.txt';


%% Sample parameter
%size of random patch
p.size_patch = [32,32,32]; 

%% Filter bank's parameters
% Number of filters in the filter bank
p.filters_no =16;

% Filter's size
p.filters_size = [11,11,11];

%% Optimization algorithm's parameters
% Number of ISTA steps on the coefficients
p.ISTA_steps_no = 5;

% Gradient step size for the feature maps
p.gd_step_size_fm = 1e-1;

% Gradient step size for the filters
p.gd_step_size_filters = 1e-5;

% Regularization's parameter
p.lambda_l1 = 2e-2;

%% Results' parameters
% Results' path: folder where the results will be saved
p.results_path = 'results';

% Number of iterations before dumping the results
p.iterations_no = 100;

% Vertical/horizontal spacing between filters in filter bank's
% representation
p.v_space = 4;
p.h_space = 4;

% Pixel size for the filters in filter bank's representation
p.pixel_size = 2;

%name of file where to save parameters
p.nameconfig = [p.results_path, '/config.mat'];

end

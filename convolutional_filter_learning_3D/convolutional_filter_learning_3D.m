function [] = convolutional_filter_learning_3D(resume_fb_no)
%  convolutional_filter_learning_3D  learns a convolutional filter bank using
%                                 the algorithm presented in [1]
%
%  Synopsis:
%     convolutional_filter_learning
%     convolutional_filter_learning(resume_fb_no)
%
%  Input:
%     resume_fb_no = when resuming a previous simulation, start from the
%                    given filter bank number (default=0, set to -1 to
%                    remove the previous simulation entirely (with 0 only
%                    img/txt dumps of the filters are removed))
%
% [1] R. Rigamonti, M. Brown, V. Lepetit "Are Sparse Representations Really
%     Relevant for Image Classification?", IEEE Conf. on Comput. Vis. and
%     Pattern Recogn., 2011

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

addpath(genpath('helper_functions'));
if (nargin==0)
    resume_fb_no = 0;
else
    if (nargin>1 || ~isnumeric(resume_fb_no))
        error('Wrong parameters');
    end
end

[p] = get_config_3D(); % <--- MODIFY HERE the algorithm's parameters

% Setup directories according to the parameters set in the configuration
% file and the iteration number that has to be resumed
[p.paths] = setup_directories(p,resume_fb_no);

% Load the dataset
[dataset] = load_3D_dataset(p);


% Initialize the filter bank
[fb] = initialize_3D_fb(p,resume_fb_no);


%size for fourier transform of filters
size_half_kernel = ceil((p.filters_size-1)/2);

%size_patch_extended = p.size_patch+p.filters_size-1;
size_patch_extended = p.size_patch+2*size_half_kernel;
%size_fft_extended = 2.^nextpow2(size_patch_extended+p.filters_size-1);
size_fft_extended = size_patch_extended+p.filters_size-1;
central_part_fm_valid_1 = p.filters_size(1)+1*(~rem(p.filters_size(1),2)):size_patch_extended(1);
central_part_fm_valid_2 = p.filters_size(2)+1*(~rem(p.filters_size(2),2)):size_patch_extended(2);
central_part_fm_valid_3 = p.filters_size(3)+1*(~rem(p.filters_size(3),2)):size_patch_extended(3);

%size for filter grad convolution
size_center_residual = p.size_patch-p.filters_size+1;
size_grad_fil = 2*p.size_patch - p.filters_size ;
%size_center_residual = p.size_patch-2*size_half_kernel 
%size_grad_fil = 2*p.size_patch - 2*size_half_kernel 
central_part_grad_fil_1 = size_center_residual(1):p.size_patch(1);
central_part_grad_fil_2 = size_center_residual(2):p.size_patch(2);
central_part_grad_fil_3 = size_center_residual(3):p.size_patch(3);
%return

% Get current iteration number as the product of the number of iterations
% required for a single dump of the filter bank and the resumed filter bank
% number
it_count = resume_fb_no*p.iterations_no+1;

% Optimize filter bank
while(true) % type Ctrl-C to stop 
    
    if (rem(it_count,p.iterations_no)==0)
        fprintf('## Iteration %d ##\n',it_count);
    end

    %% Extract a sample from the dataset
    rand_sample = randi(length(dataset));
    sample_image = dataset{rand_sample};
    
    std_patch = 0;
    std_toll = 0.02;
    tryal = 0;
    while ( std_patch < std_toll )
    
        if tryal > 200
           disp(['patch not found for image ', num2str(rand_sample)]) ;
           rand_sample = randi(length(dataset));
           %new random image :
           sample_image = dataset{rand_sample};
           tryal = 0;
        end
        
     %% Extract random patch from the sample
        start_r_rand = randi(size(sample_image,1)-p.size_patch(1));
        start_c_rand = randi(size(sample_image,2)-p.size_patch(2));
        start_s_rand = randi(size(sample_image,3)-p.size_patch(3));
        
        sample = sample_image(start_r_rand:start_r_rand+p.size_patch(1)-1,start_c_rand:start_c_rand+p.size_patch(2)-1,start_s_rand:start_s_rand+p.size_patch(3)-1);
        
        %% Verify patch is not uniform        
        std_patch = std(sample(:));
        
        tryal = tryal +1;

    end
        
    %pad sample for symmetric boundary conditions
    sample_extended = padarray(sample,size_half_kernel,'symmetric','both');
    %fft of sample 
    SAMPLE = fftn(sample_extended,size_fft_extended);
    
    %fft of filters
    FB = cell(p.filters_no,1);
    for i_filter = 1:p.filters_no,
        FB{i_filter} = fftn(fb{i_filter}(end:-1:1,end:-1:1,end:-1:1),size_fft_extended); %fft of flipped filter to compute correlation
    end

    %% Compute the feature maps for the given sample (in the frequency domain)
    % Initialize the feature maps.
    feature_maps = cell(p.filters_no,1);
    for i_fm = 1:p.filters_no
        
        F_MAPS= SAMPLE.*FB{i_fm};
        
        feature_maps{i_fm} = ifftn(F_MAPS);
        feature_maps{i_fm} = feature_maps{i_fm}(central_part_fm_valid_1,central_part_fm_valid_2,central_part_fm_valid_3);

    end

    % Perform ISTA for the given number of steps.
    % This basically accounts to a step in the direction opposite to the
    % gradient of the reconstruction error, followed by a soft-thresholding of
    % the values in the feature maps.
    % We are not interested in having a good reconstruction, that's why the
    % number of steps is small.

    for i_step = 1:p.ISTA_steps_no
        
        % Compute the reconstruction
        F_MAPS = fftn(padarray(feature_maps{1},size_half_kernel,'both','symmetric'),size_fft_extended);
        REC = F_MAPS.*FB{1};
        for i_fm = 2:length(feature_maps)
            F_MAPS = fftn(padarray(feature_maps{i_fm},size_half_kernel,'both','symmetric'),size_fft_extended);
            REC = REC+F_MAPS.*FB{i_fm};
        end
        
        % Normalize the reconstruction.
        % Empirically, we have found that dividing by filter's area gives good
        % results.
        REC = REC/prod((size(fb{1})+2));

        reconstruction = ifftn(REC);
        reconstruction=reconstruction(central_part_fm_valid_1,central_part_fm_valid_2,central_part_fm_valid_3);
        
        % Compute the residual
        residual = sample-reconstruction;

        RES = fftn(padarray(residual,size_half_kernel,'both','symmetric'),size_fft_extended);

        
        for i_fm = 1:p.filters_no
            GRAD_FM = -2*RES.*fftn(fb{i_fm},size_fft_extended);

            grad_fm = ifftn(GRAD_FM);
            grad_fm = grad_fm(central_part_fm_valid_1,central_part_fm_valid_2,central_part_fm_valid_3); 
            
            
            % Apply gradient
            fm = feature_maps{i_fm}-p.gd_step_size_fm*grad_fm;
            % Soft-thresholding
            feature_maps{i_fm} = max(abs(fm)-p.lambda_l1,0).*sign(fm);
            
        end
    end
    
   
    %% Optimize the filters
    
    %Compute reconstruction
    F_MAPS = fftn(padarray(feature_maps{1},size_half_kernel,'both','symmetric'),size_fft_extended);
    REC = F_MAPS.*FB{1};
    for i_fm = 2:length(feature_maps)
        F_MAPS = fftn(padarray(feature_maps{i_fm},size_half_kernel,'both','symmetric'),size_fft_extended);
        REC = REC+F_MAPS.*FB{i_fm};
    end
        
    % Normalize the reconstruction.
    % Empirically, we have found that dividing by filter's area+2 gives good
    % results.
    REC = REC/(size(fb{1},1)+2)^3;
        
    reconstruction = ifftn(REC);
    reconstruction=reconstruction(central_part_fm_valid_1,central_part_fm_valid_2,central_part_fm_valid_3);
        
    %Compute residual
    residual = sample-reconstruction;


    %extract central part of residual to compute gradient
    residual = residual(floor(p.filters_size(1)/2)+1:end-floor(p.filters_size(1)/2),floor(p.filters_size(2)/2)+1:end-floor(p.filters_size(2)/2),floor(p.filters_size(3)/2)+1:end-floor(p.filters_size(3)/2));
     RES = fftn(residual(end:-1:1,end:-1:1,end:-1:1), size_grad_fil);

    for i_fm = 1:p.filters_no
       FM = fftn(feature_maps{i_fm},size_grad_fil);
    
        % Compute filter's gradient as the valid correlation of the feature
        % maps with the residual        
        GRAD_FILTER = -2*FM.*RES;
        grad_filter_tot = ifftn(GRAD_FILTER);
        grad_filter =grad_filter_tot(central_part_grad_fil_1,central_part_grad_fil_2,central_part_grad_fil_3);
     

        % Update the filter
        fb{i_fm} = fb{i_fm}-p.gd_step_size_filters*grad_filter;
        
    end
    % Re-normalize the filter bank
    for i_filter = 1:p.filters_no
        fb{i_filter} = fb{i_filter}/(norm(fb{i_filter}(:)));
    end
    
    %% Eventually, dump the filter bank to a file
    if(rem(it_count,p.iterations_no)==0)
        save_filter_bank3D(p,fb,it_count);
    end
    
    it_count = it_count+1;
end

end

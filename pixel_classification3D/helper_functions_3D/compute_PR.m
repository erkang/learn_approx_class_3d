function [best_threshold] = compute_PR(PR_dir_rep,tp,fp,fn,test_img_name,recall_eval_grid)
%  compute_PR  Compute Precision/Recall curve for the given image
%
%  Synopsis:
%     [best_threshold] = compute_PR(PR_dir_rep,response,gt,test_img_name,thresholds_no,recall_eval_grid)
%
%  Input:
%     PR_dir_rep       = directory where the PR curve for the given image will
%                        be saved
%     response         = response file given by the classifier
%     gt               = ground-truth image
%     test_img_name    = name of the test image
%     thresholds_no    = number of thresholds used in ROC computation
%     recall_eval_grid = grid over which the PR will be computed
%  Output:
%     best_threshold = threshold value for which the maximum F-measure is
%                      obtained 

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

precision = tp./(tp+fp);
recall = tp./(tp+fn);

precision(1) = 1;
recall(1) = 0;
precision(end) = 0;
recall(end) = 1;

% Fit spline
prec_fit = csapi(recall,precision);
precision = fnval(prec_fit,recall_eval_grid);
precision(precision>1) = 1;
precision(precision<0) = 0;

[bestP,bestR,bestF,best_threshold] = find_best_F(precision,recall_eval_grid,recall_eval_grid);

PR_filename = fullfile(PR_dir_rep,sprintf('PR_%s.txt',test_img_name));
best_PRFT_filename = fullfile(PR_dir_rep,sprintf('best_PRFT_%s.txt',test_img_name));

fd = fopen(PR_filename,'wt');
for i_pt = 1:length(precision)
    fprintf(fd,'%f %f\n',recall_eval_grid(i_pt),precision(i_pt));
end
fclose(fd);

fd = fopen(best_PRFT_filename,'wt');
fprintf(fd,'%f %f %f %f\n',bestP,bestR,bestF,best_threshold);
fclose(fd);

end

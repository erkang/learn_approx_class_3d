function [] = compute_performances(p)
%  compute_performances  Compute the average performance in terms of ROC/PR/...
%
%  Synopsis:
%     compute_performances(p)
%
%  Input:
%     p = structure containing system's configuration and paths

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

%thresholds_no = 200;
thresholds_no =p.results_thresholds_no;
% Grids for PR and ROC computation
recall_eval_grid = linspace(0,1,thresholds_no)';
fpr_eval_grid = linspace(0,1,thresholds_no)';

for i_rep = 1:p.test_repetitions_no
resp_dir = fullfile(p.paths.responses,sprintf('rep_%03d',i_rep)); %#ok<*PFBNS,*ASGLU>
resp_list = dir(fullfile(resp_dir,'*.nrrd'));

ROC_dir_rep = fullfile(p.paths.res,'ROC',sprintf('rep_%03d',i_rep));
PR_dir_rep = fullfile(p.paths.res,'PR',sprintf('rep_%03d',i_rep));
analytical_dir_rep = fullfile(p.paths.res,'analytical',sprintf('rep_%03d',i_rep));
[status,message,messageid] = mkdir(ROC_dir_rep);  %#ok<*NASGU>
[status,message,messageid] = mkdir(PR_dir_rep); 
[status,message,messageid] = mkdir(analytical_dir_rep); 
   
for i_test_img = 1:p.test_imgs_no
    [test_img_path,test_img_name,test_img_ext] = fileparts(p.test_imgs_list{i_test_img}); 
    resp = nrrdLoad(fullfile(resp_dir,resp_list(i_test_img).name)); %
    resp = permute(resp, [2,1,3]); 

    gt = double(nrrdLoad(p.test_gt_list{i_test_img})>0);
    gt = permute(gt,[2,1,3]);
    mask = nrrdLoad(p.test_masks_list{i_test_img})>0; 
    mask = permute(mask,[2,1,3]); 
  
    gt = gt(mask==1);
    
    % Remove infinite values (useful, e.g., with EF responses which are taken as
    % logarithms) 
    resp(isinf(resp(:))) = min(min(resp(~isinf(resp(:)))));
    resp = normalize_min_max_mask(resp,mask);
    resp = resp(mask==1);
       
    %use mex file
    [tp,fp,tn,fn,thr] = roc_pr_float(single(resp),uint8(gt));
       
    compute_ROC(ROC_dir_rep,tp,fp,tn,fn,test_img_name,fpr_eval_grid);
    [best_threshold] = compute_PR(PR_dir_rep,tp,fp,fn,test_img_name,recall_eval_grid);
    compute_VI(analytical_dir_rep,resp,gt,test_img_name,best_threshold);
    compute_RI(analytical_dir_rep,resp,gt,test_img_name,best_threshold);
end
end
% ROC and PR have been computed for the whole dataset/repetition set, now
% average them
avg_precision = zeros(thresholds_no,1);
avg_precision_sq = zeros(thresholds_no,1);
avg_f = 0;
avg_f_sq = 0;
avg_tpr = zeros(thresholds_no,1);
avg_tpr_sq = zeros(thresholds_no,1);
avg_auc = 0;
avg_auc_sq = 0;
avg_RI = 0;
avg_RI_sq = 0;
avg_VI = 0;
avg_VI_sq = 0;


for i_rep = 1:p.test_repetitions_no
ROC_dir_rep = fullfile(p.paths.res,'ROC',sprintf('rep_%03d',i_rep));
PR_dir_rep = fullfile(p.paths.res,'PR',sprintf('rep_%03d',i_rep));
analytical_dir_rep = fullfile(p.paths.res,'analytical',sprintf('rep_%03d',i_rep));
for i_test_img = 1:p.test_imgs_no
[test_img_path,test_img_name,test_img_ext] = fileparts(p.test_imgs_list{i_test_img});
ROC = load(fullfile(ROC_dir_rep,sprintf('ROC_%s.txt',test_img_name)));
AUC = load(fullfile(ROC_dir_rep,sprintf('AUC_%s.txt',test_img_name)));
PR = load(fullfile(PR_dir_rep,sprintf('PR_%s.txt',test_img_name)));
best_PRFT = load(fullfile(PR_dir_rep,sprintf('best_PRFT_%s.txt',test_img_name)));
VI = load(fullfile(analytical_dir_rep,sprintf('VI_%s.txt',test_img_name)));
RI = load(fullfile(analytical_dir_rep,sprintf('RI_%s.txt',test_img_name)));

avg_precision = avg_precision+PR(:,2);
avg_precision_sq = avg_precision_sq+PR(:,2).^2;
avg_f = avg_f+best_PRFT(3);
avg_f_sq = avg_f_sq+best_PRFT(3).^2;
avg_tpr = avg_tpr+ROC(:,2);
avg_tpr_sq = avg_tpr_sq+ROC(:,2).^2;
avg_auc = avg_auc+AUC;
avg_auc_sq = avg_auc_sq+AUC.^2;

avg_RI = avg_RI+RI;
avg_RI_sq = avg_RI_sq+RI.^2;
avg_VI = avg_VI+VI;
avg_VI_sq = avg_VI_sq+VI.^2;
end
end

N = p.test_repetitions_no*p.test_imgs_no;

avg_precision = avg_precision/N;
avg_precision_sq = avg_precision_sq/N;
avg_f = avg_f/N;
avg_f_sq = avg_f_sq/N;
avg_tpr = avg_tpr/N;
avg_tpr_sq = avg_tpr_sq/N;
avg_auc = avg_auc/N;
avg_auc_sq = avg_auc_sq/N;
avg_RI = avg_RI/N;
avg_RI_sq = avg_RI_sq/N;
avg_VI = avg_VI/N;
avg_VI_sq = avg_VI_sq/N;

sigma_precision = sqrt(avg_precision_sq-avg_precision.^2);
sigma_tpr = sqrt(avg_tpr_sq-avg_tpr.^2);
sigma_f = sqrt(avg_f_sq-avg_f.^2);
sigma_auc = sqrt(avg_auc_sq-avg_auc.^2);
sigma_RI = sqrt(avg_RI_sq-avg_RI^2);
sigma_VI = sqrt(avg_VI_sq-avg_VI^2);

AVG_dir = fullfile(p.paths.res,'AVG_stats');
[status,message,messageid] = mkdir(AVG_dir);
fd = fopen(fullfile(AVG_dir,'ROC.txt'),'wt');
for i_pt = 1:thresholds_no
    fprintf(fd,'%f %f %f %f %f\n',fpr_eval_grid(i_pt),avg_tpr(i_pt),sigma_tpr(i_pt),avg_tpr(i_pt)-sigma_tpr(i_pt),avg_tpr(i_pt)+sigma_tpr(i_pt));
end
fclose(fd);

fd = fopen(fullfile(AVG_dir,'PR.txt'),'wt');
for i_pt = 1:thresholds_no
    fprintf(fd,'%f %f %f %f %f\n',recall_eval_grid(i_pt),avg_precision(i_pt),sigma_precision(i_pt),avg_precision(i_pt)-sigma_precision(i_pt),avg_precision(i_pt)+sigma_precision(i_pt));
end
fclose(fd);

fd = fopen(fullfile(AVG_dir,'AUC.txt'),'wt');
fprintf(fd,'%f %f\n',avg_auc,sigma_auc);
fclose(fd);

fd = fopen(fullfile(AVG_dir,'F.txt'),'wt');
fprintf(fd,'%f %f\n',avg_f,sigma_f);
fclose(fd);

fd = fopen(fullfile(AVG_dir,'VI.txt'),'wt');
fprintf(fd,'%f %f\n',avg_VI,sigma_VI);
fclose(fd);

fd = fopen(fullfile(AVG_dir,'RI.txt'),'wt');
fprintf(fd,'%f %f\n',avg_RI,sigma_RI);
fclose(fd);

end

function [] = compute_feature_maps_test(p,i_img)
%  compute_feature_maps_test  Compute the feature maps for the specified image
%                             from the test set
%
%  Synopsis:
%     compute_feature_maps_test(p,i_img)
%
%  Input:
%     p     = structure containing system's configuration and paths
%     i_img = image number

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

img_filename = p.test_imgs_list{i_img};
mask_filename = p.test_masks_list{i_img};

% Destination feature map's file format
fm_file_format = '%s/feature_map_3D_%03d.nrrd'; 
[img_path,img_name,img_ext] = fileparts(img_filename);
fm_output_dir = fullfile(p.paths.test_fm_dir,img_name);

% Reuse previously-computed feature maps (if their number matches the expected
% one, so as to avoid resuming incomplete sets)
if (exist(fm_output_dir,'dir'))
    file_list = dir(fm_output_dir);
    if(~(p.sep_comb_star))
        expected_file_no = p.filter_bank.no;
    else
         expected_file_no = p.filter_bank.sep_filter_no;
    end
    
    if (p.use_oof)
        expected_file_no = expected_file_no+1;
    end
    if (p.use_ef)
        expected_file_no = expected_file_no+1;
    end
    
    % +2 to account for . and .. (WARNING!! In MAC there is also .DS_Store)
    if (length(file_list)~=expected_file_no+2+1*ismac)
        warning('fm:resume','The feature maps in %s do not match in number (%d) the expected ones (%d) -- recomputing',fm_output_dir,length(file_list)-2,expected_file_no);
        [status,message,messageid] = rmdir(fm_output_dir,'s'); %#ok<*NASGU,*ASGLU>
    end
end

if (~exist(fm_output_dir,'dir'))
    fprintf('     computing the feature maps for image %s...\n',img_name);
    [status,message,messageid] = mkdir(fm_output_dir); %#ok<*NASGU,*ASGLU>
    
    fm_count = 0;
    fully_computed = false;
    % If OOF and EF are used in the current simulation, check if there is an
    % optimized version of the feature maps that has none of them
    [fm_count,fully_computed] = check_and_link(p,fm_output_dir,fm_file_format,fm_count,fully_computed,'__OOF_1_EF_1','__OOF_0_EF_0');
    [fm_count,fully_computed] = check_and_link(p,fm_output_dir,fm_file_format,fm_count,fully_computed,'__OOF_0_EF_0','__OOF_1_EF_1');
    
    img = im2double(nrrdLoad(img_filename));
    mask = im2double(nrrdLoad(mask_filename));

    %use matlab convention: first index rows, second index columns  
    img = permute(img,[2,1,3]); 
    mask = permute(mask,[2,1,3]); 
    
    
    % Compute responses from filter banks, keeping whitening into account
    if (~fully_computed && p.filter_bank.no>0)
        fm_count = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count);
    end
    
    % If requested, compute OOF response (it simply accounts to copying the
    % versione already available in the dataset)
    if (p.use_oof)
        oof_response = im2double(nrrdLoad(p.test_oof_list{i_img}));
        oof_response = permute(oof_response,[2,1,3]);
        oof_response = normalize_in_mask(oof_response,mask);
        
        oof_response(mask==0) = 0;
        nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),permute(oof_response,[2,1,3]));
        fm_count = fm_count+1;
    end
   
    % If requested, compute EF response (it simply accounts to copying the
    % versione already available in the dataset)
     if (p.use_ef)
         ef_response = im2double(nrrdLoad(p.test_ef_list{i_img}));
         ef_response = permute(ef_response,[2,1,3]);
         ef_response = normalize_in_mask(ef_response,mask);
        
         ef_response(mask==0) = 0;
         nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),permute(ef_response,[2,1,3]));
         fm_count = fm_count+1;
     end
else
    
    fprintf('     the feature maps for image %s have already been computed, skipping...\n',img_name);
end

end

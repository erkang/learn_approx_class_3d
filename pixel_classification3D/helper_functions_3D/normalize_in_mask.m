function [normalized] = normalize_in_mask(in_matrix,mask)
%  normalize_in_mask  Normalizes an input matrix according to a mask,
%                     subtracting the mean and dividing by the average of the
%                     input inside the mask
%
%  Synopsis:
%     [normalized] = normalize_in_mask(in_matrix,mask)
%
%  Input:
%     in_matrix = matrix (or image) to be normalized
%     mask      = mask to be applied in the normalization process
%  Output:
%     normalized = normalized version of the input matrix

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

scan_area = in_matrix(mask>0);
if (std(scan_area(:))>1e-6)
    normalized = (in_matrix-mean(scan_area(:)))/std(scan_area(:));
else
    normalized = zeros(size(in_matrix));
end

end

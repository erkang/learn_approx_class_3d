function [] = test_RF(results_path,model,test_imgs_list,test_filenames,i_rep)
%  test_RF  Test a trained Random Forest classifier on the given test data
%
%  Synopsis:
%     test_RF(results_path,model,test_imgs_list,test_filenames,i_rep)
%
%  Input:
%     results_path   = path for storing the segmented images
%     model          = trained Random Forest classifier
%     test_imgs_list = list of file names for the test images (used to generate
%                      the filename of the segmented images)
%     test_filenames = list of files containing the feature maps in a format
%                      suitable for the classifier
%     i_rep          = iteration number (for multiple random tests)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

fprintf('  Testing Random Forest classifier on input data...\n');

[status,message,messageid] = mkdir(fullfile(results_path,sprintf('rep_%03d',i_rep))); %#ok<ASGLU>

parfor i_test_file = 1:length(test_filenames)
    [test_img_path,test_img_name,test_img_ext] = fileparts(test_imgs_list{i_test_file}); %#ok<*NASGU,ASGLU>
    test_data = load(test_filenames{i_test_file});
    
    % The last column contains the test labels
    features = test_data(:,1:end-1);
    
    votes = vigraPredictProbabilitiesRF(model,features);

    % Dump the result in NRRD format
    result_filename = fullfile(results_path,sprintf('rep_%03d',i_rep),sprintf('%s.nrrd',test_img_name));
   
    test_img = nrrdLoad(test_imgs_list{i_test_file});
    size_test_img = size(test_img);
    
    output = reshape(votes(:,2), [size_test_img(3),size_test_img(1),size_test_img(2)]);

    %output = zeros(size_test_img);
    %for i_row = 1:size_test_img(1)
    %    for i_col = 1:size_test_img(2)
    %        for i_sli = 1:size_test_img(3)
    %            output(i_row,i_col,i_sli) = votes(size_test_img(3)*( size_test_img(2)*(i_row-1) + i_col-1 ) + i_sli,2); %%%3D ???
    %        end
    %    end
    %end
    nrrdSave(result_filename,permute(output,[2,3,1]));

end

end

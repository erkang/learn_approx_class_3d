function [normalized] = normalize_min_max_mask(in_matrix,mask)
%  normalize_min_max_mask  Normalizes an input matrix according to a mask,
%                          rescaling the image in [0,1]
%
%  Synopsis:
%     [normalized] = normalize_min_max_mask(in_matrix,mask)
%
%  Input:
%     in_matrix = matrix (or image) to be normalized
%     mask      = mask to be applied in the normalization process
%  Output:
%     normalized = normalized version of the input matrix

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

scan_area = in_matrix(mask>0);
min_val = min(scan_area(:));
max_val = max(scan_area(:));
if (abs(max_val-min_val)>1e-5)
    normalized = (in_matrix-min_val)/(max_val-min_val);
else
    normalized = zeros(size(in_matrix));
end

normalized(mask==0) = 0;

end

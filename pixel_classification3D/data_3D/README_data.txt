------- Dataset Folder -------

Create here a folder <DATASET_NAME> where to put the files containing your dataset.
This folder should contain the following files: 

- dilat_sampling_mask_3D.txt: list of files containing a generic mask for sampling train data (e.g. gt dilatated);
- neg_sampling_mask_3D.txt: list of files containing the masks of the negative samples;
- pos_sampling_mask_3D.txt: list of files containing the masks of the positive samples;
- test_ef_3D.txt (optional): list of files containing EF output on test images;
- test_gt_3D.txt: list of files containing ground truth of test images;
- test_imgs_3D.txt: list of images used for test;
- test_masks_3D.txt: list of masks of test images (used to test algorithm only in a part of the image);
- test_oof_3D.txt (optional): list of files containing OOF output on test images;
- train_ef_3D.txt (optional): list of files containing EF output on train images;
- train_gt_3D.txt: list of files containing ground truth of train images;
- train_imgs_3D.txt: list of images used for training;
- train_masks_3D.txt: list of masks of train images (used to train algorithm only in a part of the image);
- train_oof_3D.txt (optional): list of files containing OOF output on test images;

N.B. All the images listed in the files above should have .nrrd extension.

The filter bank used should be saved as .txt file in <DATASET_NAME>/filter_banks_3D/.
Please follow the following conventions:
1) If the filter bank is not separable
The .txt file should contain the whole filter bank saved piling up the rows.
More precisely, if N is the number of learned filters and R,C,S are respectively the number rows, columns and slices of the filters, then the .txt file should have C columns and R*S*N rows, where the first R*S rows correspond to the first filter (first row corresponding to first row of the first slice, second row to the second row of the first slice, … , R-th row to the last row of the first slice, …, R*S-th row corresponding to the last row of the last slice), rows from R*S+1 to 2*R*S correspond to the second filter and so on. 

2) If the filter bank is separable:
if K is the number of separable filters and R,C,S are respectively the number of rows, columns and slices of the filters, then the .txt file should have K columns and R+C+S rows, where if we define A the matrix formed by the first R rows, B is the matrix formed by rows from R+1 to R+S and C is the matrix formed by the last S rows, then the k-th separable filter s_k is given by: s_k = A(:,k)oB(:,k)oC(:,k), where 'o' denotes the vector outer product.

3) If the filter bank is separable and used to approximate a full rank filter bank (as described in [1]):
the separable filter bank should be saved in a .txt file as described in 2) and the weights used for the reconstruction should be given in a .txt file containing a KxN matrix W, where K is the number of separable filters and N the number of filters in the original filter bank. W is such that f_n = sum_k(W(k,n)*s_k), where f_n is the n-th of the original filters and s_k the k-th of the separable filters.


[1] Learning separable filters
